/// <reference types="Cypress" />

import { Counter } from "../src/Counter";

describe("Test counter", () => {
  let counter;
  beforeEach(() => {
    counter = new Counter();
  });
  it("Should increment", () => {
    counter.increment();
    expect(counter.count).to.equal(1);
  });
  it("Should decrement", () => {
    counter.count = 5;
    counter.decrement();
    expect(counter.count).to.equal(4);
  });
  it("Should not decrement beneth zero", () => {
    counter.decrement();
    expect(counter.count).to.equal(0);
  });
  it("Should reset", () => {
    counter.count = 5;
    counter.reset();
    expect(counter.count).to.equal(0);
  });
});

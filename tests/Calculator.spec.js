/// <reference types="Cypress" />
import { Calculator } from "../src/Calculator";

describe('Test calculator', () =>{
    let calc;
    beforeEach(()=> {
        calc = new Calculator();
    });
    
    it('Should add two numbers', () =>{
        let result = calc.add(2,2);

        expect(result).to.equal(4);
    });
    it("Should throw error if NaN given", () => {

      expect(() => calc.add('bloup', 2)).to.throw();
    });
})
export class Counter {
    constructor(number){
        this.count = 0;
    }
    increment(){
      this.count++;
    }
    decrement(){
        if(this.count === 0){
            this.count = 0;
        }else{
            this.count--;
        }
    }
    reset(){
        this.count = 0;
    }
}
export class Calculator {
    
    /**
     * Method that calculs...
     * @param {number} a first number
     * @param {number} b secong number
     * @returns {number} total result
     */
    add(a,b){
        // if(Number.inNaN(a) || Number.inNaN(b))
        if(typeof(a) !== 'number' || typeof(b) !== 'number'){
            throw new Error('Incorrect argument. Number expected.')
        }
        return a + b;
    }
}